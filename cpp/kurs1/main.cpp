#include <iostream>
#include <math.h>
#include <stdio.h>
// �������
#include <windows.h>
#include <time.h>
#include <iomanip>

// ������ ������ ��� �����
#define NW 10
// ���-�� ������ ����� �������
#define NP 3

using namespace std;

void enterMatrix     (float **matrix, int n, int m);
void matrixOut       (float **matrix, int n, int m);
void matrixGausSolve (float **matrix, int n, int m);


int main()
{
	setlocale(LC_CTYPE, "rus");
	srand(time(NULL));
	cout.setf(ios::fixed);
	cout.precision(NP);
    int n, m;

	cout << "������� ����� ���������: ";
    cin >> n;
	m = n + 1;
	// ���������� m, ��� �� �������� �� 1 ������ ������
	// ��� ����� ��� �������������� ���������
    float **matrix = new float *[m];
    // ������� ���. ������ (������) � ������� ��� ��������������� ���������
    matrix[n] = new float [n-1];

	enterMatrix(matrix, n, m);
	cout << "\n\n�������� �������: \n\n";
	matrixOut(matrix, n, m);

    //�������� �������

    matrixGausSolve(matrix, n, m);

    // ������ �� �����
    for (int i=0; i==n; i++)
        delete []matrix[i];
    delete []matrix;

	system("PAUSE");
}


void enterMatrix(float **matrix, int n, int m)
{
	for (int i = 0; i < n; i++) {
		matrix[i] = new float [m];
		for (int j = 0; j < m; j++) {
			cout << "������� " << "[" << i+1 << " , " << j+1 << "] = ";
			cin >> matrix[i][j];
			//matrix[i][j] = rand() % 100 - 50;
		}
	}
}


void matrixOut(float **matrix, int n, int m)
{
    for (int i=0; i<n; i++)
    {
        for (int j=0; j<m; j++) {
            cout << setw(NW) << matrix[i][j] << " ";
        }
    cout << endl;
    }
}


void matrixGausSolve(float **matrix, int n, int m)
{
	int maxLine=0;
	float tmp=0;

    cout << "\n\n\n********************************\n"\
            "********* ����� ����� **********\n"\
            "****** � ������� �������� ******\n"\
            "*********** �������� ***********\n"\
            "********************************\n\n";
    for(int c = 0; c < n-1; c++) {
        //���� ��������
        for (int i = c; i < n; i++)
            if (abs(matrix[maxLine][c]) < abs(matrix[i][c]))
                maxLine = i;

        cout << "a[" << maxLine + 1  << "][" << c + 1 << "] ���������� �� ������:\n\n";
        matrixOut(matrix, n, m);

        if (matrix[maxLine][c] == 0) {
            cout << "\n!!! ������� �� ����� ������� !!!\n\n";
            return;
        }

        //���� ������ �� �� ������ �����, �� ������ �� � ������
        if (maxLine != c){
            cout << "\n\n������ " << maxLine + 1 << " � " << c + 1 << " ������ �������:\n\n";
            for (int j = c; j < m; j++) {
                tmp = matrix[c][j];
                matrix[c][j] = matrix[maxLine][j];
                matrix[maxLine][j] = tmp;
            }
            matrixOut(matrix, n, m);
        } else {
            cout << "\n������� ������ �� ���������";
        }

        cout << "\n\n��������������(��) ���������(�) ��� " << c + 1 << " �������:\n\n";
        for (int j = c; j < n - 1; j++) {
            matrix[n][j] = matrix[j + 1][c] / matrix [c][c];
            cout << setw(NW) << matrix[n][j] << " ";
        }

        cout << "\n\n\n����������� �������:\n\n";
        for (int i = c + 1; i < n; i++) {
            for (int j = c; j < m; j++) {
                matrix[i][j] = matrix[i][j] - matrix [c][j] * matrix[n][i-1];
                //cout << setw(NW) << matrix[i][j] << " ";
            }
        }
        matrixOut(matrix, n, m);

        cout << "_____________________________\n\n\n";
    }
}
